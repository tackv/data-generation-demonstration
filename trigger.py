
import openhtf as htf
from openhtf.plugs.user_input import UserInput

class InfoPlug(UserInput):
    """ This our custom Plug that inherits from UserInput.
    """
    FORM_LAYOUT = {
        'schema':{
            'title': "Testbench info",
            'type': "object",
            'required': ["uutid", "operator", "test", "product"],
            'properties': {
                'uutid': {
                    'type': "string", 
                    'title': "UUT ID"
                },
                'operator': {
                    'type': "string", 
                    'title': "Operator"
                },
                'test': {
                    'type': "string", 
                    'title': "Test to execute"
                },
                'product': {
                    'type': "string", 
                    'title': "Product"
                },
            }
        },
        'layout':[
            "uutid",
            {
            "key": "operator",
            "type": "select",
            "titleMap": [
                { "value": "Robert", "name": "Robert" },
                { "value": "John", "name": "John" },
                { "value": "Thomas", "name": "Thomas"}
            ]
            },
            {
            "key": "test",
            "type": "select",
            "titleMap": [
                { "value": "Number Generation", "name": "Number Generation" },
                { "value": "Log Generation", "name": "Log Generation" },
                { "value": "Number & Log Generation", "name": "Number & Log Generation"}
            ]
            },
            "product",
        ]
    }

    def prompt_tester_information(self):
        self.__response = self.prompt_form(self.FORM_LAYOUT)
        return self.__response

    def greet_tester(self):
        try:
            self.prompt('Hello {uutid} {operator} !'.format(**self.__response))
        except AttributeError:
            # self.__response does not exist. prompt_tester_information was never called during this test.
            raise Exception("Cannot greet tester before prompt_tester_information")

        
