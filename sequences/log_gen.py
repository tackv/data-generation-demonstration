import openhtf as htf
from spintop_openhtf import TestPlan, TestSequence
from test_case_library import GaussianTest, LogTest, GammaTest


def create_log_generation_sequence():

    sequence = TestSequence('Log Generation Sequence')
    
    @sequence.testcase('Log_Test')
    def execute_log_test(test):
        test.logger.info(LogTest.LogTest())
	
    return sequence
    
    