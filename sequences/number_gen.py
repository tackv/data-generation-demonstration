import openhtf as htf
from spintop_openhtf import TestPlan, TestSequence
from test_case_library import GaussianTest, LogTest, GammaTest
from criteria.criteria import create_gaussian_criteria, create_gamma_criteria
from products.product import Product


def create_number_generation_sequence():

    sequence = TestSequence('Number Generation Sequence')

    sub_seq = sequence.sub_sequence('Gaussian Sub Sequence')
    @sub_seq.testcase('Gaussian_Test_1', requires_state=True)
    def execute_gaussian_test_1(state):
        """Verifies gaussian generated numbers are within """

        measurements = state.test_api.measurements

        product_gaussian = Product.create_product(state.test_api.state["product"])

        values = GaussianTest.GaussianTest(0, product_gaussian, state.test_api.state["operator"])
        
        # Generate first measure criteria from base value
        state.running_phase_state.measurements['GAUSSIAN_1_MEASURE_1'] = create_gaussian_criteria('GAUSSIAN_1_MEASURE_1', product_gaussian.BASE)

        # Generate other measure criteria from other measures
        state.running_phase_state.measurements['GAUSSIAN_1_MEASURE_2'] = create_gaussian_criteria('GAUSSIAN_1_MEASURE_2', values[0] + product_gaussian.STEP)
        state.running_phase_state.measurements['GAUSSIAN_1_MEASURE_3'] = create_gaussian_criteria('GAUSSIAN_1_MEASURE_3', values[1] + product_gaussian.STEP)
        state.running_phase_state.measurements['GAUSSIAN_1_MEASURE_4'] = create_gaussian_criteria('GAUSSIAN_1_MEASURE_4', values[2] + product_gaussian.STEP)
        state.running_phase_state.measurements['GAUSSIAN_1_MEASURE_5'] = create_gaussian_criteria('GAUSSIAN_1_MEASURE_5', values[3] + product_gaussian.STEP)

        #Evaluate Criteria
        measurements['GAUSSIAN_1_MEASURE_1']  = values[0]
        measurements['GAUSSIAN_1_MEASURE_2']  = values[1]
        measurements['GAUSSIAN_1_MEASURE_3']  = values[2]
        measurements['GAUSSIAN_1_MEASURE_4']  = values[3]
        measurements['GAUSSIAN_1_MEASURE_5']  = values[4]
        
    

    return sequence


