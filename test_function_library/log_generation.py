from numpy import random

def generate_int (max):

	x = random.rand()
	y = x * max
	return round(y)
	
def generate_log (outlier=100, erroneous=10):

	logstr = "This is the log string to verify resulting in PASS"

	int = generate_int(outlier)
	if int == outlier:
		logstr = "This is the oultlier log string"
	else:
		int = generate_int(erroneous)
		if int == erroneous: 
			logstr = "This is the log string to verify resulting in FAIL"
		else:
			logstr = "This is the log string to verify resulting in PASS"
			
	return logstr