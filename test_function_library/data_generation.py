
from numpy import random

def generate_int (max):

	x = random.rand()
	y = x * max
	return round(y)

def setup_gaussian (operator, config, gaussian):

	if operator not in config.Operators.LIST:
		outlier = 20
		stdev = 1.2
	else:
		outlier = config.Gaussian.OUTLIER_RATE[operator]
		stdev = config.Gaussian.BASE_STDEV[operator]
		
	int = generate_int(outlier)
	
	if int == outlier:	
		x = random.normal(loc=0, scale=stdev)
		print ("Outlier")
	else:
		x = random.normal(loc=gaussian.BASE, scale=stdev)
		
	return round(x,2)
		
def generate_gaussian (target=20, stdev=0.5):

	x = random.normal(loc=target, scale=stdev)		
	return round(x,2)
	
def generate_gamma (target=20, shape=12, outlier=100):

	int = generate_int(outlier)
	if int == outlier:
		x = 0
	else:
		x = target + random.gamma(shape)
	
	return round(x,2)