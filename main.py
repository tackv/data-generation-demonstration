# main.py
import os

import openhtf as htf
from openhtf.plugs.user_input import UserInput
from openhtf.util import conf

from spintop_openhtf import TestPlan, TestSequence

from trigger import InfoPlug

from sequences.number_gen import create_number_generation_sequence 
from sequences.log_gen import create_log_generation_sequence

""" Test Plan """

def test_demo_plan_factory():
    return TestPlan('test-demo', store_location='prod_tests_results/{dut_id}-{start_time_millis}')

plan = test_demo_plan_factory()

            
@plan.trigger('Testbench Info')
@plan.plug(info=InfoPlug) # Use our custom plug. key ('greet') defines the keyword of the function
def testbench_info(test, info): # keyword 'infp' matches with @plug

    response = info.prompt_tester_information()

    define_test_state(
        test, 
        dut_id=response['uutid'], 
        operator=response['operator'], 
        test_type=response['test'], 
        product=response['product']
    )
        
def define_test_state(test, dut_id, operator, test_type, product):
    test.dut_id = dut_id
    test.state["operator"] = operator
    test.state["test"] = test_type
    test.state["product"] = product

    if test.state["test"] == "Number Generation" or test.state["test"] == "Number & Log Generation":
        test.state["do_number"] = True
    else:
        test.state["do_number"] = False

    if test.state["test"] == "Log Generation" or test.state["test"] == "Number & Log Generation":
        test.state["do_log"] = True
    else:
        test.state["do_log"] = False


test_sequence = TestSequence('test-sequence')

test_sequence.append(create_number_generation_sequence())
test_sequence.append(create_log_generation_sequence())

plan.append(test_sequence)

def create_non_interactive_plan(**test_state):
    non_interactive_plan = test_demo_plan_factory()

    @non_interactive_plan.testcase('init')
    def init_non_interactive(test):
        define_test_state(test, **test_state)
    
    non_interactive_plan.append(test_sequence)
    non_interactive_plan.no_trigger()
    return non_interactive_plan

if __name__ == '__main__':
    #plan.enable_spintop()
    #conf.load(spintop_org_id='tackv')
    plan.run()
	
	
	