
import random
import yaml
import os
from datetime import timedelta

import dateutil
import simpy
from numpy.random import normal

from .prod_test import run_test
from .data_analysis_pipeline import DataGenPipeline

HERE = os.path.abspath(os.path.dirname(__file__))
PROD_DATA_FILENAME = os.path.join(HERE, 'prod_data.yml')

def simulate(start_datetime, prod_data_filename):
    with open(prod_data_filename) as prod_data_file:
        prod_data = yaml.safe_load(prod_data_file)

    records = prod_batch('Batch 1', start_datetime, prod_data, [f'dut{idx}' for idx in range(10)])

    pipeline = DataGenPipeline()
    pipeline(*records)
    pipeline.apply_views()


def prod_batch(batch_name, start_datetime, config, duts):
    records = []
    prod_test_config = config['prod_test']

    operators = [operator['name'] for operator in prod_test_config['operators']]

    for dut in duts:
        record = run_test(start_datetime=start_datetime, dut_id=dut, operator=random.sample(operators, 1)[0])
        record.add_tag('batch', batch_name)
        records.append(record)
        start_datetime += timedelta(seconds=random_wait_time(
                prod_test_config['average_time_between_steps'], 
                prod_test_config['std_dev_between_steps']
            )
        )
    return records

def random_wait_time(average_time_between_steps, std_dev_between_steps):
    wait_time = normal(average_time_between_steps, std_dev_between_steps)
    return wait_time

if __name__ == '__main__':
    simulate(dateutil.parser.parse('2020-01-01 12:00:00'), PROD_DATA_FILENAME)

