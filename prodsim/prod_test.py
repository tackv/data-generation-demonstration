import dateutil

from spintop_openhtf.transforms.openhtf_fmt import OpenHTFTestRecordTransformer

from main import create_non_interactive_plan
from .data_analysis_pipeline import DataGenPipeline

openhtf_to_spintop_transform = OpenHTFTestRecordTransformer()

def run_test(start_datetime, dut_id='dut1', operator='Robert', test_type="Number & Log Generation", product="A"):
    records = []

    def on_test_done(test_record):
        try:
            duration = test_record.end_time_millis - test_record.start_time_millis
            test_record.start_time_millis = start_datetime.timestamp() * 1000
            test_record.end_time_millis = test_record.start_time_millis + duration

            records.append(openhtf_to_spintop_transform(test_record))
        except:
            import traceback
            traceback.print_exc()
            raise

    plan = create_non_interactive_plan(
        dut_id=dut_id, 
        operator=operator,
        test_type=test_type,
        product=product
    )
    plan.add_callbacks(on_test_done)
    plan.run_console(once=True)

    return records[0]


if __name__ == '__main__':
    run_test(start_datetime=dateutil.parser.parse('2020-01-01 12:00:00'))