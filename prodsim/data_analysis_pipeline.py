import dotenv
dotenv.load_dotenv()

from apache_beam.transforms import ParDo

from spintop_da.beam_api.pipelines import BeamPipeline
from spintop_da.beam_api.steps import DoWrapRecordMessage

class DataGenPipeline(BeamPipeline):
    def expand(self, all_records):

        # Sink 1: Process Steps and Features
        self.collect_lifecycle_steps(all_records)

    def collect_lifecycle_steps(self, pcoll):
        return (
            pcoll
            | 'Wrap records' >> ParDo(DoWrapRecordMessage())
            | 'Lifecycle router' >> self.env.lifecycle_main_router()
        )

    def apply_views(self):
        functions = self.env.analytics_factory().functions

        # First apply stream to last view
        query = functions.map_stream_to_latest_views('steps_stream', extract_tags=['bucket'])
        query.update_view('steps')

        query = functions.map_stream_to_latest_views('features_stream')
        query.update_view('features')

        # Create transitions
        query = functions.lc_multi_transitions('steps', ('step',))
        query.materialize_query('transitions').result()

        # Create measure view
        query = functions.array_measure_declarative_values_mart(
            'features',
            declarative_measures={
                'Gaussian1': dict(
                    keys=dict(measure=range(1,6)),
                    name_format='GAUSSIAN_1_MEASURE_{measure}'
                )
            }
        )
        query.materialize_query('test_demo_measures').result()
    


