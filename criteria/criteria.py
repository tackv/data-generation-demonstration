import openhtf as htf

from products.product_A import Gaussian as GaussianCrit

def create_gaussian_criteria(name, target):
	GAUSSIAN_MARGIN = 2
	return htf.Measurement('{}'.format(name)).    in_range(target - GAUSSIAN_MARGIN, target + GAUSSIAN_MARGIN)


def create_gamma_criteria(name):
	return htf.Measurement(name).    in_range(76, 1000)

