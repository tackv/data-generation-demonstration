class Product():

    def create_product(name):
        
        if name == "B":
            from products.product_B import Gaussian as GaussianConfig
        else:
            from products.product_A import Gaussian as GaussianConfig
        
        return GaussianConfig()