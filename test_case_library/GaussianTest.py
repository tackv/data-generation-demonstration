
from test_function_library import data_generation
from static import static_config

##Gaussian Test
def GaussianTest(index, product_gaussian, operator):

	base = data_generation.setup_gaussian(operator, static_config, product_gaussian)

	value_array = []
	value = data_generation.generate_gaussian(target=base + index, stdev=product_gaussian.STDEV)
	value_array.append(value)

	value = data_generation.generate_gaussian(target=value + index + product_gaussian.STEP, stdev=product_gaussian.STDEV)
	value_array.append(value)

	value = data_generation.generate_gaussian(target=value + index + product_gaussian.STEP, stdev=product_gaussian.STDEV)
	value_array.append(value)

	value = data_generation.generate_gaussian(target=value + index + product_gaussian.STEP, stdev=product_gaussian.STDEV)
	value_array.append(value)

	value = data_generation.generate_gaussian(target=value + index + product_gaussian.STEP, stdev=product_gaussian.STDEV)
	value_array.append(value)


	return value_array
	


	