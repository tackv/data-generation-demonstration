
from test_function_library import data_generation
from static import static_config

##Gamma Test
def GammaTest(index):

		
		value = data_generation.generate_gamma(	target=static_config.Gamma.TARGET + index, 
												shape=static_config.Gamma.SHAPE, 
												outlier=static_config.Gamma.OUTLIER_RATE)
									
		return value																						
												
	