
class Operators():
	LIST = ["Robert", "John", "Thomas"]

class Gaussian():
	BASE_STDEV = {
			"Robert": 1,
			"John": 1.2,
			"Thomas":2
	}
	OUTLIER_RATE = {
			"Robert": 50,
			"John": 8,
			"Thomas":50
	}
	
class Gamma():
	TARGET = 70
	OUTLIER_RATE = 100
	SHAPE = 20

class Log():
	OUTLIER_RATE = 100
	ERRONEOUS_RATE = 10